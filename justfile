pub: blog links
    rm -rf public
    mkdir -p public/
    mkdir -p public/airband
    mkdir -p public/blog
    mkdir -p public/ha
    mkdir -p public/links
    cp -r site-airband/* public/airband/
    cp -r site-homepage/* public/
    cp -r site-ha/* public/ha/
    cp -r site-links/output/* public/links/
    cp -r site-blog/output/* public/blog/

blog:
    cd site-blog && invoke publish

links:
    cd site-links && cargo run 

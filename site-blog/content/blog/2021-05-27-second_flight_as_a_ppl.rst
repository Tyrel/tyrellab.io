May 27, 2021 - Second flight as a PPL
#####################################
:author: tyrel
:category: flying
:tags: flying
:status: published

I was able to sneak in a cooler morning flight in N8080A. I took my motorcycle to the airport, but forgot my yoke mount so I was iPad-less. We had to wait for a long while to get fuel because the services truck was filling the HU-16 that had arrived a few days beforehand. There was also a C-27 taking off, super cute, Feels like a baby C-130!

.. figure:: {static}/images/2021/05/20210527_hu16.jpg
   :alt: Grumman HU-16 Albatross

   Grumman HU-16 Albatross

.. figure:: {static}/images/2021/05/20210527_c27.jpg
   :alt: Alenia C-27J Spartan

   Alenia C-27J Spartan

We went up, took a few minutes to do another 45° bank turn. I did that a little better, less shaky. Then he had me practice some emergency descents. I didn't do as well at those as I should. I need faster ADM skills. That will come with time, one of my big weaknesses is emergency things, so it's one thing I'm excited to start practicing.

After the emergency procedures, he had me practice rudder control. Putting in enough rudder while turning to hold on target before the turn and rolling out while pointing at a target - but rolling out. I'm much better at rolling out than rolling in, so I'm excited to have a new procedure to practice. One of the things I like a lot about this CFI giving me the checkout to borrow planes, is that he's young. He doesn't have these really old ways of thinking about things and I appreciate that.

After the higher altitude stuff, he wanted to see some short-field landings. The first time in the pattern we had to extend our downwind A LOT, there were three people on long finals, so our downwind was funky and long. I'm still getting used to this plane and the runway so by the time we were about to touch down, the other plane in front of us was still on the runway (albeit turning onto taxiway A3 a mile down the runway) so he said go around, as technically the other plane was still on the runway and things could go wrong even with that far separation.

Did a go around and the next two short fields were okay. We got off at Taxiway A2 I think, and then took taxiway A back to the ramp. When we got to the intersection with A and A1 there was another plane getting ready to cross the threshold, but it was a Cessna 172… high wing! They didn't notice that there was a taildragger apparently norad coming on a 45° final, not straight in and almost rolled into the runway. Luckily the CFI I was with shouted STOP on the CTAF and they stopped, seconds before an incursion! EEEEK!

Wrapped up, and planned our third checkout ride, it'll be down to KFAY, a controlled field. Should be on June 2nd and then I'll be cleared to rent the planes and feel like a full member of the Wings of Carolina!

Unfortunately no GPX file, but I have a screenshot.


.. figure:: {static}/images/2021/05/20210527_track.png
   :alt: GPX Track across the Raleigh Durham area west of Jordan Lake

   GPX Track across the Raleigh Durham area west of Jordan Lake


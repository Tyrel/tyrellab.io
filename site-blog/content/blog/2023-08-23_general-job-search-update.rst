General Job Search Update
#########################
:author: tyrel
:category: Personal
:tags: work
:status: published

As mentioned in a previous post, I'm on the job hunt again.
I got laid off in June with a 30% Reduction In Force.

I've been searching primarily for Python and Go roles, but I'm not having a lot of luck right now - seems everyone else also got laid off is who I am competing against.
`(That said, go hire my friend Nik! He's fantastic!) <https://nkantar.com/blog/2023/08/hire-me-v202308/>`_
I've had a lot of job opportunity people ghost me.
Gotten through a few late rounds, only to never hear from the company again.
Even if I have emailed them a thank you letter for the interviews, to express my interest.

I've been around professionally for thirteen years.
Over those years, I have picked up mostly back end skills.
I have eight solid years of Django experience.
Four years of Ruby on Rails experience.
A couple years of Flask, FastAPI, and other smaller Python Frameworks mixed in.

I'm looking for an IC role, where I can move into a Tech Lead role. I want to eventually some day be a Staff/Principal role, but I don't have that on paper to show I can do it, so trying to get in somewhere new with an IC role.

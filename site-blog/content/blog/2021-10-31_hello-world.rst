Hello, world!
#############
:date: 2021-10-31 19:10
:author: tyrel
:category: Blog
:slug: hello-world
:status: published

This blog here I want to keep some permanent tech thoughts in more written form than my `Wiki <https://tyrel.website/wiki/>`__ where I keep the majority of my written work. I do have a `flight blog <https://k3tas.radio/airband>`__, which has a lot of customization for maps and such that I don't want on here.

I have revived some old blog posts from previous blogs, to give some life to this blog while I'm writing new articles.

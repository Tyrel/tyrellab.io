6502 NES Course by Pikuma
#########################
:author: tyrel
:category: Tech
:tags: 6502, assembly, NES
:status: published

As I mentioned in my `December <https://tyrel.dev/blog/2022/12/advent-of-code-2022-end-of-year-updates.html>`_ post I'm doing a 6502 course on `Pikuma. <https://pikuma.com/courses/nes-game-programming-tutorial>`_

I'm about 75% of the way done, and I think I need to circle back to some earlier stuff about how the PPU works, but it's super fun.

Over the holidays I was able to stop at my father's and pick up my old NES.
I swapped out the ZIF connector for a new one, and cleaned up some contacts on the RCA ports, and it works great!
Once I found out that it was working - I played Sesame Street ABC 123, as that's the only one I had up in my office - I ordered an EverDrive N8.
That came last week.

The pictures are tall due to how I took them, so sorry I'll attach them at the end of the post.

Once I got the `EverDrive N8 <https://krikzz.com/our-products/legacy/edn8-72pin.html>`_ I made sure it worked by playing a Battletoads ROM.
Battletoad tested - I then copied Atlantico.NES to my Everdrive.
Atlantico is the game that Gustavo is walking us through making in the current part of the course - not a real published game.
I loaded it up and HOLY COW - something I actually wrote in Assembly is running on real hardware.

If you want to watch the video, it's very simplistic at the 75% mark, this was before the Collisions chapter, and no sound yet.

The feeling of getting something running, locally, and seeing it working on screen, despite being a programmer for ~~20 years, is AMAZING.
Writing code that executes on the system you grew up playing the early 90's, wow.

I do wish the CRT TV my wife had was square, things get cut off on it.
I even got a remote, so I could try to fix that in the menu, alas, only picture option is brightness.
(Not that I realistically thought I could scale it, CRT Pixels are only Pixels.

----

Picture Gallery
===============

Trying out putting all the pictures at the end of my posts, if they are not directly related to paragraph content.

.. figure:: {static}/images/2023/01/NES_Console.png
   :alt: An NES Console with the flap opened up. On top is a Zapper gun with another controller, both with cables neatly wrapped up. On the floor, plugged in, in front of the NES, is another controller. The Power LED is on.

   My NES plugged in and running.

.. figure:: {static}/images/2023/01/NES_Atlantico.png
   :alt: A CRT TV screen with a game running on it. In 8bit graphics, the game is a ship sailing to the right, with planes flying to the left. Some missiles are shooting up.

   Atlantico game. There may be some interference with scanlines causing moire patterns, sorry.

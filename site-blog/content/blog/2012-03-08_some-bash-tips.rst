Some BASH tips
##############
:date: 2012-03-08 03:56
:author: tyrel
:category: Tech
:tags: bash, linux
:slug: some-bash-tips
:status: published

I realize I haven't updated in a while. I haven't had much free time recently as I've been working on a project for my father in C# after work hours. This is a great change from only working in Python and JavaScript recently. I'm making a program that will analyze test results from a plasma torch for a company called HyperTherm. My father built the physical machine, but the employees want something that they can better see the results of a passed torch unit, or a failed torch unit. This program has a bar code scanner that scans the tool used in the test and matches it up to the lot of torch parts. Another added feature is the ability to print a white label that says "UNIT PASSED" or a giant red label that says the unit failed and which of the 8 tests failed were.I had to learn how to use delegates, as my serial event listener is on a separate thread and I can't update labels, or parts of the User Interface without them. Still working on it, hopefully will wrap it up by Saint Patrick's day.


I recently found a cool command in BASH that I hadn't previously known. ``C-o`` will execute the current line, and then bring the following line up from BASH history. If you have a set of commands you want to execute again, rather than having to press up 20 times, hit enter, press up 19 times, hit enter, and so on… You can just hit up 20 times. Press C-o as many times as you need to.

For example:

.. code-block:: console

	$ touch a
	$ touch b
	$ touch c
	# [up] [up] [up]
	$ touch a [C-o]
	$ touch b [C-o]
	$ touch c [C-o]

As you can see there, all I had to do was go back to the ``$ touch a`` line, and hit control-o three times and it touched the files again!

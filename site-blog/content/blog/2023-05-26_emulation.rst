Emulation
#########
:author: tyrel
:category: Tech
:tags: emulation
:status: published

I haven't had much time lately for blog posts, I've been dealing with bed time routines with my newborn, and once those are done, I get a few hours of alone time for computer things.

Lately I've been toying around with Amiga OS, FreeDOS, Windows 95, and Apple IIe things.

I got a raspberry pi and installed Pimiga, got a fun set up and that was neat. I then installed Amiberry on my macbook, with some remote hard drive images on my samba share, and I have a consistent setup for Amiga on any machine in the house or on tailscale.

I then decided to install FreeDOS to a barely used Dell Vostro 1720 and install to that. It works great, I have WordStar, TurboC, and more installed and it's fun to get back to my roots in that way.

After that I decided to install Windows 95 with 86Box, did the same with remote hard disk images, and got that running. Been toying around in Visual Basic 6, Oh the memories!! I installed that so I could play Lego City, but having voodoo graphics errors I need to figure out before I can play.

I also ordered an Apple IIe emulator machine that runs on an Esp8266 from `CT6502 <https://ct6502.org/product/mfa2-32emu/>`_ and it works great. So cool just tossing a disk image on the MicroSD card and loading it up. The downside to this is I can't figure out how to swap disks in realtime, so I can't play Ultima, or any multi disk games. I can however load .hdv files so if something comes with a hard disk image.

Not really much for a tech post, and nothing to share codewise, but thought I'd break some radio silence. I also imported my flying blog here, so I added the Flying category/tags.



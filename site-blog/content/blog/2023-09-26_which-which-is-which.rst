Which which is which?
#####################
:author: tyrel
:category: Tech
:tags: linux, macos, zsh
:status: published

I had a bit of a "Tyrel you know nothing" moment today with some commandline tooling. 

I have been an avid user of ZSH for a decade now, but recently I tried to swap to fish shell.
Along the years, I've maintained a lot of different iterations of `dotfiles <https://gitea.tyrel.dev/tyrel/dotfiles>`_, and shell aliases/functions.
I was talking to a `friend <https://fredeb.dev>`_ [citation needed] about updating from ``exa`` to ``eza`` and then noticed I didn't have my aliases loaded, so I was still using ``ls`` directly, as I have ``alias ls="exa -lhFgxUm --git --time-style long-iso --group-directories-first"`` in my ``.shell_aliases`` file.

I did this by showing the following output: 

.. code-block:: shell

  $ which ls
  /usr/bin/ls

Because I expected it to show me which alias was being pointed to by ``ls``.

My friend pointed out that "Which doesn't show aliases, it only points to files" to which I replied along the lines of "What? No way, I've used ``which`` to show me aliases and functions loads of times."

And promptly sent a screenshot of my system NOT showing that for other aliases I have set up. Things then got conversational and me being confused, to the point of me questioning if "Had I ever successfully done that? Maybe my macbook is set up differrently" and went and grabbed that.

Friend then looked at the man page for which, and noticed that there's the ``--read-alias`` and ``--read-functions`` flags on ``which``, and I didn't have those set.
I then swapped over to bash "Maybe it's a bash thing only? I'm using Fish".

Nope, still nothing! Then went to google, and it turns out that ZSH is what has this setup by default.
Thank you `"Althorion" <https://stackoverflow.com/a/14196212>`_ from Stackoverflow for settling my "Yes you've done this before" confusion.

It turns out that ZSH's ``which`` is equivalent to the ZSH shell built-in ``whence -c`` which shows aliases and functions.

After running ``/usr/bin/zsh`` and sourcing my aliases (I don't have a zshrc file anymore, I need to set that back up), I was able to settle my fears and prove to myself that I wasn't making things up. There is a which which shows you which aliases you have set up, which is default for ZSH.

.. code-block:: shell

   $ which ls
   ls: aliased to exa -lhFgxUm --git --time-style long-iso --group-directories-first

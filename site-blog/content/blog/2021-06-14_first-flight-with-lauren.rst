First Flight With Lauren
########################
:author: tyrel
:category: flying
:tags: flying
:status: published

Today was my first flight with my wife, Lauren. She is my first real passenger ("technically your DPE is, blah blah"). I didn't know what to expect, I knew she had been up in planes before - she's gone skydiving, something I will probably never do. I'd rather be the person to fly the "JUMPERS AWAY OVER X" than be a jumper. I don't want to make this entire blog about Lauren, although we all know I could. So I will try to keep it 50% airplane talk!

We scheduled a 12pm flight, with an estimate of me probably taking off at 12:30 (took off at 12:40 for pattern and 12:49 to KDAN), Cross country from KTTA to KDAN and back to KTTA. I wanted to fly at 4500'msl there, and 5500'msl back, following the East is Odd +500 and West is Even +500, because it was a 358°ish flight there and a 178°ish back (that's from airport center to airport center, but I did some cloud avoidance, a few times (like, the whole time) so I was pretty off course, but ForeFlight will get me there. I brought my Stratux because I remembered that N8116J didn't like bluetooth and ADSB so my iPad wouldn't connect to the plane. I also bought a new GPYes unit that I wanted to try out, and it worked great, no more magnetic GPS sticking to random magnetic things, everything is self contained in my Stratux!

Wind was good for RWY21, so I had to taxi via Taxiway Alfa to get there, still not a fan of how Wings of Carolina has us do a runup off the taxiway, something I need to get used to at a busier airport than KEEN. Well, we took off and I did a loop of the pattern - I wanted to gauge Lauren's stomach, and my flying abilities to make sure she was okay to fly. The Pressure Density was 2,800'-2,900, which for a 255'msl runway, oooof. After the second takeoff we flew out to the traffic pattern then departed to the north! We climbed up to 3000, the cloud layer was about 3800 so I chose to stay under it, vs fly over the top and chance not having an opening. It was pretty bumpy - there's a convective SIGMET over like the whole east coast - so I knew it was going to be a little bumpy. Lauren was taking pictures the whole time (she took 47! I'll share some) and got some fun ones of me. In my pictures I look like I'm super concentration face, but she got a couple of me smiling.

Nearing KDAN I realized that the CTAF was Actually a CTAF (Common Traffic Advisory Frequency) and that KDAN had a tiny little tower. I called 15 to the south (because I heard a lot of traffic) and the nice person on the frequency came in and said something like 16J there's two planes on the taxiway, one in the pattern and one just departing, which now I understand why people come on our Unicom back at KEEN and keep saying "KEEN Unicom please advise". Well we got closer I said I was flying over midfield to make a tear drop into the downwind. I did as such, flew over at 2500', did the tear drop, down to 1500' and landed. We then taxied to the FBO and shut down [my outside camera picked THREE seconds after I shut down to die, perfect timing]. The nice ramp assistant gave us a ride to the FBO in the golf cart so we could pee.

Gatorade passed through our system completely we headed out to the plane. We got some weird looks from the people chilling in the FBO (Enterprise people, and like a grandfather? idk) for wearing masks. Yes we're both vaccinated, but I sure as hell don't know if the random people I run into are, gotta keep you safe!

We took a few pictures, and then went and started up the plane. When I was doing my run up, I didn't have the mixture full rich, was still leaned for ground ops, so when I did the mag drop I heard a backfire, was like OH SHIT and realized I was leaned. Put in full mixture, and did the mag drops again, ~150rpm loss and we were good to go. Lesson learned here is even if there are shortcuts for when the engine is hot, and you just land for 10 minutes, probably good to go through the full checklist even just skimming it.

We took down taxiway Alfa again and got to RWY02. Side note, KEEN has 02-20 and 14-32, this airport KDAN has 02-20 and 13-31 so when I came upon it, I was like "holycrap I feel like I'm home". Anyway - when we got to the end of the taxiway there were two entrances only like 100ft apart to the runway, it was weird. We had to wait like 10 minutes to take off, because there were 4 planes doing touch and goes and they were perfectly spaced to give me like NO time to take off. I know how long of a runway I need to be safe, but with a 2900' Density Altitude, I know I needed a longer ground roll, so finding time to slip in to where I knew I could take off was annoying. Finally one of the guys on downwind said "Plane waiting at 02, I'm extending my downwind a little bit to give you some time to take off, so I rolled out, thanked him and took off to the north a bit. I extended my upwind and gave the pattern a large buffer, then took off to the south.

Lauren seemed to be getting tired by like 75% of the trip back so maybe next time I'll take the plane for four hours, and then we can take a full hour between legs, instead of 15 minutes. I did three hours, because I planned for a longer break between - we even bought zucchini bread I made last night! - but I threw in that extra pattern lap before we left the airport, so that added like 15 minutes, and I couldn't find the flight book so that took some extra time.

When we were on the way back to Raleigh Exec, we saw another plane like 500' below us so I flew above them and circled past them then tried to come in behind them but I have no idea where they went when I flew over them, my ADSB decided to not pick them up, so I did a larger right turn so I knew I'd avoid them. It was weird and I'll have to check FlightRadar24 playback to see - but it's not up now for some reason. After that steep turn I entered the downwind and landed. We landed with 2 minutes to spare, but it took a few minutes to shut down, and tie down. Glad someone wasn't right after us (if someone was I would not have gone as far of course). Covered the plane, paid the $260 for 2.3 hours of flying, put the book away and headed home.

The return trip took about exactly the same length. We left KTTA at 12:49, landed at KDAN at 1:36 (49min). Then with waiting for takeoff, we left KDAN at 2:07 and landed at 2:57(50min). On the flight back, it was a bit bumpier, the clouds were still scattered at about 3800 but some were darker than others so I flew around them. I wonder if when I finally fly IFR if I'll fly a lot straighter, instead of just "oops avoid that cloud!" that remains to be seen.


.. figure:: {static}/images/2021/06/14_tyrel-passenger-seat.jpg
   :alt: Tyrel sitting in passenger seat, waiting for Lauren to get into plane.

   Tyrel sitting in passenger seat, waiting for Lauren to get into plane.

.. figure:: {static}/images/2021/06/14_powerlines.jpg
   :alt: Power line swath through the trees, with a nuclear power plant in the disance. 

   Power line swath through the trees, with a nuclear power plant in the disance. 


.. figure:: {static}/images/2021/06/14_tyrel-looking-left.jpg
   :alt: Tyrel looking to the left, watching for traffic 

   Tyrel looking to the left, watching for traffic 

.. figure:: {static}/images/2021/06/14_danville-VA.jpg
   :alt: Danville, Virginia airport, from above. 

   Danville, Virginia airport, from above. 

.. figure:: {static}/images/2021/06/14-clouds.jpg
   :alt: Clouds and the right wing of a Piper Warrior airplane 

   Clouds and the right wing of a Piper Warrior airplane 


.. figure:: {static}/images/2021/06/14_tyrel-pulling.jpg
   :alt: Tyrel pulling the plane forward, to get it lined up to push back for parking.

   Tyrel pulling the plane forward, to get it lined up to push back for parking. 

.. figure:: {static}/images/2021/06/14_TTA.gif
   :alt:  TTA Airpot Diagram

   Airport Diagram of TTA

.. figure:: {static}/images/2021/06/14_DAN.gif
   :alt: DAN Airport Diagram

   Airport Diagram of DAN

WOC Final Checkout Ride
#######################
:author: tyrel
:category: flying
:tags: flying
:status: published

I can finally rent planes through Wings of Carolina! Flew today and the CFI passed me.

I was a bit nervous about the North Carolina weather so I text the instructor to see if we could leave an hour early for the controlled airspace portion of the checkout. He said let's shoot for 3:30, we took off at 4 because he gave me a bit of ground instruction on the systems in N8116J. (Also we couldn't figure out the radio, so had to push a few buttons to get comms working. It seems when 16J was in the shop, they had undone all the COM1 COM2 selection buttons, which caught us off guard.)

We took off, then flew south to Fayetteville (KFAY), Luke showed me the auto pilot, how to climb, descend, turn to a heading, so we enabled that on the flight to KFAY. 20 miles north, called approach, I fumbled my radio A LOT. I definitely need more controlled airspace practice. ATC Talks FAST. It's hard to fly the plane and copy things down, I think I need to start using a pen and paper, not Foreflight for ATC remarks, it'll be much easier. I put a FieldNotes book in my flight bag the other day, time to use it! After a few more fumbles (I copied back the altimeter, not the altitude, talk about stress!) I was cleared to land. Squawked 0210 and landed on runway 22 almost straight in, I took a right handed base, it was weird! There were no commercial flights so they were at a lull of traffic which is why I just got the RWY22 CLEAR TO LAND. Unfortunately (or Fortunately..) LIVE ATC and KFAY approach are down so I have no recording of my fumbles. Would have been nice to hear them again so I could learn.

We landed, taxied back to runway 22, and then said we were taking off VFR. Squawked 0212 now and departed, stayed runway heading until they said turn right heading 320 staying at or below 2500. Then a bit later they said fly flight plan heading I turned 355 and then we finally got out of their airspace. There's two airports there, KFAY and Simmons Army Airfield (KFBG), so their airspace looks like a cell dividing, two round circles and of course we bisected it the long way.

Out of their airspace Luke said "I have the controls", then banked hard 60 degrees right, and pointed down. He said "If you're ever VFR and inadvertently hit IMC weather, and get into an unusual attitude, hit this button [LVL] and presto, the wings will level." which of course they did. Auto pilot is still wild to me, I never flew with it in N43337, so it's going to take a lot to get used to, but I'm sure I'll start loving it.

We then called 10mi on the 45 for RWY22, did a touch and go, (I came in a little higher than I wanted I still need to learn the sight pictures for this airport, but oof it's FLAT still.) took off did the pattern once more and did a full stop.

On this flight, I learned the GPS a little bit more, the touch screen menu will make selecting comms so much easier, glad they have these consistently in all their planes. I also learned AutoPilot, I need to find a flight sim model with this auto pilot (I did get a Logitech/Saitek Multi Panel with AutoPilot on it this weekend, so good timing!) that I can learn how the IAS, ALT, VS, HDG buttons work better. HDG and ALT I get, those just hold heading and alt, but airspeed and vertical speed climbs are still magical!

Luke then signed me off, so I'm clear to rent any of the PA-28-161 that they have at Wings of Carolina. I told him I want to learn how to fly the Mooney M20J soon, I'm excited to get my complex endorsement.

CFEngine3 Install on CentOS 5.7
###############################
:date: 2012-05-25 03:57
:author: tyrel
:category: Outdated
:tags: cfengine, centos
:slug: cfengine3-install-on-centos-5-7
:status: published

| Today I was tasked with installing CFEngine3 on CentOS-5.7 (A little outdated). When installing CFEngine-3.3.1 I kept getting an error that I couldn't find libtokyocabinet.so.9. I had to set my prefix to /usr/ because the location that tokyocabinet was installing my libraries to was not being read by CFEngine's make script.
| To do this (I am using tokyocabinet 1.4.47)

.. code-block:: console

   wget http://fallabs.com/tokyocabinet/tokyocabinet-1.4.47.tar.gz
   tar -xzvf tokyocabinet-1.4.47.tar.gz
   cd tokyocabinet-1.4.47/
   ./configure --prefix=/usr/
   make
   sudo make install

Then I was able to ./configure && make && make install cfengine3 without any problems.

So my overall script looked something like this:

.. code-block:: console

   wget http://fallabs.com/tokyocabinet/tokyocabinet-1.4.47.tar.gz
   tar -xzvf tokyocabinet-1.4.47.tar.gz
   cd tokyocabinet-1.4.47/
   ./configure --prefix=/usr/
   make
   sudo make install

.. code-block:: console

   wget http://cfengine.com/source-code/download?file=cfengine-3.3.1.tar.gz
   tar -xzvf cfengine-3.3.1.tar.gz
   cd cfengine-3.3.1
   ./configure
   make
   sudo make install
   sudo cp /var/cfengine/bin/cf-* /usr/bin/

You can un-expire a GPG key.
############################
:date: 2012-01-13 03:54
:author: tyrel
:category: Tech
:tags: linux, gpg
:slug: you-can-un-expire-a-gpg-key
:status: published

Today we had a problem at work on a system.
Without getting into too much detail as to give away secrets behind the verbal NDA I am behind, I will just say that it had to do with a GPG public key of mine that was expired on a dev machine, accidentally propagating during install to a production machine.
This key had a sub key as well, so figuring out this was tricky.

To start, you can list your gpg keys like so:

.. code-block:: console

  $ gpg --list-keys


This will list keys such as

.. code-block::  console

  pub 4096R/01A53981 2011-11-09 [expires: 2016-11-07]
  uid Tyrel Anthony Souza (Five year key for email.)
  sub 4096R/C482F56D 2011-11-09 [expires: 2016-11-07]

To make this not expire, (same steps to change expiration date to another time), you must first edit the key

.. code-block:: console
  
  $ gpg --edit-key 01A53981


You will then see a gpg prompt ``gpg>``

Type "expire" in and you will be prompted for how long to change it to


.. code-block:: console

  Changing expiration time for the primary key.
  Please specify how long the key should be valid.
  0 = key does not expire
  <n> = key expires in n days
  <n>w = key expires in n weeks
  <n>m = key expires in n months
  <n>y = key expires in n years

You are then done setting the expiration on the primary key, if you have sub key, doing this is as easy as typing ``key 1`` and repeating the expiration step.

To finish and wrap things up, type ``save`` and you are done.

Ganymede, Twilio
################
:date: 2012-05-04 23:30
:author: tyrel
:category: Tech
:tags: nodejs, twilio
:slug: ganymede-twilio
:status: published

Last night I wrote the beginnings of my first NodeJS application. Is application even the correct word?

I've been meaning to try out the cool API by Twilio, which is used for SMS and VoiceCalling. I decided to design a system that will be two+ endpoints. One is the main server which will listen for UDP messages. When it receives the correct UDP message, configured in the config(`konphyg <https://github.com/pgte/konphyg>`_) files, it will fire off a message to Twilio and send me a text message.

The next steps, which I should be getting to tonight, are to create the Arduino portion and the serial listener. The Arduino will have a button that will send a message over serial to another NodeJS listener. This will decide if the press was good enough, if it passes the debouncing filter, and then fire a message to the main Ganymede server.

This could be used as a little text message doorbell for when you have your music on too loud. I don't believe I will ever sell this, as it's just for me to get into NodeJS, but It would be fun to share with friends.

The source so far is located on my github at [DEADLINK].

I will write more as the project continues about the different technologies and comment on my choices in the source a little bit.

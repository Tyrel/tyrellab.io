Mooney M20J checkout... Sort of
###############################
:author: tyrel
:category: flying
:tags: flying
:status: published

Tuesday night I emailed my instructor saying "Hey the Mooney is available Sunday afternoon, could we start getting me ready for a checkout", and he said "sure, book it", so I did. Then last night I started reading through the POH. There's a lot of neat differences. Cowl flaps, retractable landing gear, constant speed prop. I definitely wanted to start getting my complex rating. I read through that POH, and look up a lot of videos on how to work the prop. I'm used to just throttle and mixture in the Warriors, but this has throttle, manifold pressure, and mixture levers. I learned the cool parts about how the oil pressure, and springs set the propeller angle.

So I get to the airport at about 1pm, and we start talking about what I know. I explained how the undercarriage stuff works, under 132kias for lowering, and under 107kias for raising, and learned from Luke that there's two different speeds because of gravity and fighting gravity. We spend the first hour talking about why you need different manifold pressures. Then we head out to the plane and do a very comprehensive pre-check. I learned that the M20J doesn't have a trim tab, the whole tail raises and lowers on a pivot, NEAT! The flaps are suuuper wide and the ailerons are kind of thin. The landing gear was an interesting inspection because of how the poles and hinges open/close the gear.

After the pre-flight we get flying. Things are pretty similar on the ground, as the propeller lever is pushed all the way forward, and the throttle is the main control. Luke points out that the propeller is rounded, so there's an RPM range that you shouldn't idle in, (1550-1950ish), vs the squarer prop that the other Mooney had.

We take off, and one thing I check different is if I have available runway left, and when I don't, I can raise the landing gear at that point. It was pretty cloudy, so we had to climb up to 5500′, and eventually we hit 7000′ to jump over some clouds. First time really being above clouds this much, for the first hour we were over them. For the climb out he told me to set the plane to "twenty-five squared" - 2500rpm and 25in/hg. And every so often I would need to add more manifold pressure because of the atmosphere lapse rate while climbing.

We got up to 6000′ ish, initially and then did some straight and level flying, super smooth above the clouds today. Then we did some 30 degree turns, did a few of those okay. When we did 45 degree turns, my first left one was kind of bad - lost a lot of altitude. One to the right was okay, and then the third one to the left I kept my altitude right.

After turns we did slow flight, and it's pretty much the same controls. The power off stalls I was I guess putting in unconscious left aileron, because we kept starting to spin a little to the left, next time I fly I definitely need to practice that more.

Well we ended up going pretty far east, at one point we were like 15 miles southeast of RDU, felt uncomfortable not talking to anyone there, but I guess it was out of the normal flight path for the jets so we were fine.We ended up needing to head back to the airport for 4pm, so I put the plane in a 750fpm descent while heading back, at one point I was able to punch through the clouds at around 2500′, and we joined the traffic pattern on the 45 for downwind on 21. I landed VERY well he said. He said that people transitioning from Warriors to Mooneys land hard because the M20J is a lot lower of a plane than the PA28, so they flare early and land hard. I didn't flare early both times we landed so he said we could just end for the day. I always love when instructors say I do well with landings!

We then get back to the airport to do some payments and find out "Oh, Tyrel needs 250 hours without an instrument rating, to fly the M20J, or 150 with an instrument rating." So Oops, I guess it'll be a while before I can rent this, and get an actual check out.

An Update On Flying
###################
:date: 2022-05-07 01:15
:author: tyrel
:category: Personal
:tags: flying
:slug: an-update-on-flying
:status: published

I took ten years to get my pilot's license. From March 17, 2010 to December 30, 2020. It was amazing. I now find myself a year and a half later from achieving my goal and I don't find myself interested enough right now to go flying. There's a gas crisis, there's a pandemic, there's a lot of political things going on, a war in Ukraine, that it kind of feels bad wasting hundreds of dollars just going sight seeing.

I just completed a ground school course for my Instrument Rating -- I still need to take the written test part. With that out of the way I can start actually flying with an instructor to get my instrument rating. One of the planes the club that I am a part of has is a `Mooney M20J <http://wingsofcarolina.org/aircraft>`__. This requires 250 hours of flight time, or 100 hours and an instrument rating. I'm at that annoying 145 hour mark that dissuades me from wasting 100 hours just to fly that plane, and wanting to get my instrument rating.

I left my previous job last December so I didn't have the excess money to fly for a month while job hunting, and well, habit becomes habit... I haven't flown since `October <https://k3tas.radio/airband/2021/10/18/october-17-2021-back-above-keene/>`__! I'm definitely in a better place now, with a much nicer job and salary though. I'm hoping to maybe pick it back up again this fall. I wasn't pleased AT ALL (those who follow me on twitter will probably know this, online class is not the environment for me) with the ground school over Zoom, so I want to redo this by watching the Sporty's ground school. I need to put aside some time over the next coming weeks to actually sit down and watch it. Hopefully I can start flying with an instructor soon. I'm not looking forward to taking the written test, as I have to go visit a testing center at RDU airport - so there is that kind of delaying me too.

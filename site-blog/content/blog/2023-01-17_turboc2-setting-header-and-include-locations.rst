TurboC2 Setting Header and Include Locations
############################################
:author: tyrel
:category: DOS
:tags: DOS, DOSBox, TurboC2, C
:status: published

This weekend I purchased a book from this seller on Craigslist - `"Advanced MS-DOS Programming: The Microsoft Guide for Assembly Language and C Programmers" <https://www.librarything.com/work/178384>`_ and before opening it, I wanted to get a C environment running.

I found a copy of `TurboC2 on Archive.org <https://archive.org/details/msdos_borland_turbo_c_2.01>`_ and tossed that into my DOS Box install. I wrote a "Hello world" and pressed compile and it couldn't include "stdio.h", what the heck?

It seems that the Archive.org copy of Turbo C 2 ships with configuration that sets where the Includes and Lib directories to ``C:\TC``. I keep all my programs in ``C:\PROGS`` so of course it can't find any header files for me!

To fix this you can either move your TurboC install to ``C:\TC``, which feels wrong to me, or you could configure it in the options properly.



Steps
~~~~~

* Go to the Directories entry in the Options Menu.
* You can see the default provided configuration directories
* Fill out your appropriate directories for all three of the options.
* Make sure all three are configured properly.
* Then you can save your config, so you only have to do this once.


The Screenshot Way
~~~~~~~~~~~~~~~~~~

.. figure:: {static}/images/2023/01/dosbox_1_environment_menu.png
   :alt: TurboC with the Options Menu selected and the Directories entry highlighted.

   Go to the Directories entry in the Options Menu.

.. figure:: {static}/images/2023/01/dosbox_2_directories.png
   :alt: TurboC with a pop up, showing entries of Include Directories, Library Directories, and Turbo C Directory configured to C:\TC\INCLUDE C:\TC\LIB and C:\TC

   You can see the default provided configuration directories
   
.. figure:: {static}/images/2023/01/dosbox_3_directories_edit.png
   :alt: TurboC with a pop up, showing C:\PROGS\TC\INCLUDE

   Fill out your appropriate directories for all three of the options.

.. figure:: {static}/images/2023/01/dosbox_4_directories_filled.png
   :alt: TurboC with a pop up, showing entries of Include Directories, Library Directories, and Turbo C Directory configured to C:\PROGS\TC\INCLUDE C:\PROGS\TC\LIB and C:\PROGS\TC

   Make sure all three are configured properly.

.. figure:: {static}/images/2023/01/dosbox_5_save_config.png
   :alt: TurboC with a popup showing a Save Options location of C:\PROGS\TC\TCCONFIG.TC

   Then you can save your config, so you only have to do this once.

Unfortunately - this file is a binary file. You can't just edit it in a text editor and carry on, so this is the only way I know how to change these locations.

Hopefully this helps anyone else who runs into any include errors with Borland Turbo C 2!

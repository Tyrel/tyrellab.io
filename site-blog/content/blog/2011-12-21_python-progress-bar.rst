Python Progress Bar
###################
:date: 2011-12-21 03:52
:author: tyrel
:category: Tech
:tags: Python
:slug: python-progress-bar
:status: published

I was looking for a nice progress bar today at work to show progress rather than just printing "\ **Waiting 30 seconds…**\ " and having the script do nothing, I wanted to have a progress bar show.

I found a progress bar from `Corey Goldberg <http://code.google.com/p/corey-projects/source/browse/trunk/python2/progress_bar.py>`__

I did make a couple changes, and have uploaded my changes to my GitHub account.

newPythonProgressBar [deadlink]

To use this progressbar, it is very easy.

.. code-block:: python

   # To Setup
   from progress_bar import ProgressBar
   import sys
   import time
   def updateBar(step):
       p.update_time(step)
       sys.stdout.write("%s\r" % p)
       sys.stdout.flush()
   #
   # to call
   #
   wait_time = 100 # seconds
   p = ProgressBar(wait_time)
   p.empty_char = "."
   p.unit = "^"
   for step in range(wait_time+1):
       updateBar(step)
       time.sleep(1)

It will look like this when you use it

``[###...............7%..................] 7^/100^``

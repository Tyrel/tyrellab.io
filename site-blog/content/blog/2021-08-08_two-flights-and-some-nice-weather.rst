Two flights and some nice weather
#################################
:author: tyrel
:category: flying
:tags: flying
:status: published

The other day there was a call to action on the Wings of Carolina slack. "Is anyone able to help this pilot get one of our planes back?" Seems this newly licensed pilot, (Congrats!) passed his check ride on Friday and the weather was not great so had to leave N69012 at Asheboro. I said I would be glad to help out if the IFR weather cleared by 9am, I could give him a ride over in N8080A.

I booked a 9am-12pm block, and got to the airport at 8:55am. Grabbed the 80A book book and went to the lounge to check weather. Luckily the fog had burned off and the skies were clear! The pilot met me in the lounge, I wrapped up the weather briefing (nothing of note) and we went out to preflight. The left red nav light was out, but that's only required at night, confirmed by asking the other pilot. The fuel truck came and filled us up to the tabs (34 gallons) and we were off.

My flight plans were from KTTA -> KHBI (drop off pilot) -> KEXX then back to KTTA. Just under a 2 hour round trip, which took a bit longer because when we got to the hold short line for RWY03, I realized my iPad wasn't connected to the GPS, so we fiddled with it for a few (no one was behind us, but oof Hobbs running) and couldn't get it working. That's fine, we had our GPS and the pilot knew what KHBI looked like form the air. We took off, departed the pattern to the north, got to a little bit higher then turned west.

The pilot was super helpful, having a copilot be able to put in the radios for Siler City as we passed, and the AWOS/CTAFs in for KHBI was actually a really big load off my plate. It's the little things! Flew at about 3000ft over to KHBI because It was only a 20 minute flight so I stayed low. We get to the airport and he lets me know that RWY24 has a papi, so we chose that runway to land on. Land pretty okay, didn't grease it, but it is what it is. Taxi over and get him to his plane. I shut down so he can get out safely, then realize after he got out, I CAN'T OPEN THE DOOR. I had to yell him over to make sure I could open the door again, the bottom latch was stuck! I recorded a video, then instagrammed a `fellow pilot who is famous in his circles for getting stuck in his plane and having to call someone down in the area who landed and helped him out <https://www.instagram.com/p/CLnMONphlsu/>`_. Told him he's not alone, hah.

Anyway, I was able to open the door so I felt like it was okay if I crashed and had to open the door myself. I turn the plane back on, plug in my Stratux for ADSB on iPad, and get ready to take off. I taxi over to the runway (took 24 again out) and hold short for a taildragger and another plane to land. I then take off and woahh there were like 80 birds at the end of the runway at like 300ft up. Luckily I was able to scare them and flew above them, then departed to the right (west) and headed to KEXX. Luckily thr CTAF is the same for both (122.8) so I was able to hear the traffic without fiddling with the radio. I set the AWOS beforehand, so I checked that when I was close, and still winds calm, not bad.

Got 10 miles, out, announced position, 5 miles out, announced I was entering the downwind at a 45. I was on the down when a pilot to my right asked if I was on base, kind of weirded out because I wasn't so I was vigilant and said I was on downwind (again…). Then called base, and final and landed. Final was neat at Davidson county, had to fly over a factory, which was kind of cool.

Landed, taxied back to the runway and lined up and waited again. The taildragger had followed me! Saw him land as I was at the hold short line. Waited for a jet to land and then off I went! I climbed out, departed to the east, and climbed up to 5500msl. Held 5500 very well this trip, the air was super calm and I'm getting better at electric trim, much easier than just wheel trim.

At one point I turn the auto pilot on, with it set to 109° and 5500msl, but for some reason it made me descend and banked me left 45° and I wasn't liking that so I disengaged it. I climbed up back to 5500' for the rest of my trip. By the time I was back to KTTA, the winds still favored RWY03, so I joined the pattern on a 45° entry, and landed smoothly. At around 3000msl descending near Ashbury it was a bit bumpy, but that's the only turbulence I experienced. Such a nice morning.

Well I get back to the airport and land and shut down… I'm stuck again! Door won't open. There was no one around I could yell to, so I called the front desk, no answer. I tried again, using different pressures and trying to see if there was like a little latch that didn't hook. I fiddled for a few minutes and was able to get out, I guess I got it just right.

I then cover the plane, and squawk the door not opening, and posted the video on slack. The next pilot replied to my slack post later that evening that he had no problems. Maybe I'm just door cursed.

Lauren and I then went to lunch before my next flight where I was taking her up in a Cessna 152 (N89333). Well we do the preflight dance, go taxi and take off. At like 300' above the runway, the damn pilot side door opens up! It's fine but I don't want to worry my wife, on her second flight with me ever, so I have her hold the door closed while I go finish the pattern loop (I was doing that anyway). Well MORE fun happened the pilots PTT(push to talk) button got stuck off, so I had to use her microphone to talk, (I could have just switched the plugs, but we were in the base turn). I kind of overshot base so had to over correct a little bit, but we landed okay, taxied back and agreed we were discontinuing our flight. We could have gone back up and carried on - we had the allotted time - but you know what? The rest of the flight wasn't in the cards.

Video of me trapped in The Warrior - `https://youtu.be/0uAL30nCuYE <https://youtu.be/0uAL30nCuYE>`_.


.. figure:: {static}/images/2021/08/08_cockpit_selfie.jpg
   :alt: Selfie in the cockpit, me wearing a blue shirt.

   Cockpit Selfie

.. figure:: {static}/images/2021/08/08_behind_left_wing.jpg
   :alt: A look at the ground behind the left wing

   A look at the ground behind the left wing

.. figure:: {static}/images/2021/08/08_behind_right_wing.jpg
   :alt: Looking at a runway in the distance, not sure which one now.

   Looking at a runway in the distance, not sure which one now.

.. figure:: {static}/images/2021/08/08_hazy-highway.jpg
   :alt: Looking in front of the left wing, at a highway, the sky is hazy

   Looking in front of the left wing, at a highway, the sky is hazy

.. figure:: {static}/images/2021/08/08_hazy_runway.jpg
   :alt: Hazy sky with a runway a couple miles away.

   Hazy sky with a runway a couple miles away.


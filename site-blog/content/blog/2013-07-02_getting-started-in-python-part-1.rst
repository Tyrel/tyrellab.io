Getting started in Python Part 1
################################
:date: 2013-07-02 03:59
:author: tyrel
:category: Tech
:tags: python, pip, virtualenv
:slug: getting-started-in-python-part-1
:status: published

I have a friend who is interested in becoming a Python developer. He has some Python experience with CodeAcademy, but he of course wants to take this a step further and develop on his own computer. I figure I'd give him a few pointers, and I know this has been rehashed a million times, but what the hell, why not blog on it again.
There are a few important things to learn besides the actual language itself. The first I am going to discuss is has to deal with installing packages, then followed up closely with Python's path trickery. Finally I'm going to wrap up by discussing some software related to development, that could be used for any language, but I use daily in my work as a Python Software Engineer. Let's get started.

PIP
---

Python is a wonderful language, but how useful would it be if you had to rewrite everything by hand? Not useful at all. That's why the lovely pip developers were born. `PIP <https://pypi.python.org/pypi/pip>`__ (executable pip) is a package manager written for Python. It's very simple to use, and in my opinion is way better than easy_install. To use pip you need to know at a minimum three commands.

``pip install``

This command does exactly what it says on the box. It queries PyPI (Python Package Index) and downloads the latest version of the package on the server. It then installs it to your site-packages.

``pip uninstall``

This deletes all files associated with the package supplied. 100% simple.

``pip freeze``
This shows what packages are installed on your system and what versions. If you supply ‐‐local it will show what packages are installed in your current environment.
These three commands will get you started with package management, there are more commands you can find by looking through the help documents.

Virtualenv
----------

If you notice I mentioned a current environment in my previous ``pip freeze`` explanation, here is why. Python has a default place that it looks when you reference a package. This is generally in something like ``/usr/lib/python2.7/site-packages/`` or ``C:\Python27\lib``. There is a set of scripts called ``virtualenv`` that creates an environment where you run it with a complete copy of your Python executable, and a blank (unless you copy them over) site-packages directory. You can then install any packages there activate the virtual environment. When activated you use those specific versions, no matter the version of what is installed on your system.

Let's show an example of the first time use of ``virtualenv``:


.. code-block:: console

  $ sudo pip install virtualenv # Only time you might need sudo, try without first.
  $ virtualenv myenv # Create the virtual environment
  $ source myenv/bin/activate # Activate the virtual environment
  (myenv)$ python -c "import MYPACKAGE; print MYPACKAGE"

Notice how it says your package is not in ``/usr/lib/python2.7/site-packages/`` ? That's because you're using ``virtualenv`` to tell your copied python to use that library instead. There are many reasons you would want to use a virtual environment. The most frequent reason is to preserve version numbers of installed packages between a production and a development environment. Another reason virtualenv is useful if you do not have the power to install packages on your system, you can create a virtual environment and install them there.

Virtualenvwrapper
-----------------

After you create a virtual environment, you just run``source bin/activate`` and it will activate the virtual environment. This can get tedious knowing exactly where your virtual environments are all the time, so some developers wrote some awesome scripts to fix that problem. This is called``virtualenvwrapper`` and once you use it once, you will always want to use it more. What it does is that it has you create a hidden directory in your home directory, set that to an environment variable and references that directory as the basis for your virtual environments. The installation of this is pretty easy, you can``pip install virtualenvwrapper`` if you want, or download the package and compile by hand. 

Once installed correctly, you can run the command ``mkvirtualenv envname`` to create a virtual environment. You can then run``workon envname`` from anywhere, and it will activate that environment. For example, you could be at``/var/www/vhosts/www.mysite.com/django/`` and run``workon envname`` and it would activate the environment from there. This isn't a required package (none of them are really…) as I went a couple years without using``virtualenvwrapper``, but it is very useful and now I use it every day. Some tips I use with my setup of``virtualenvwrapper`` is that I use the postactivate scripts to automatically try to change into the proper project directory of my environment. This also means I usually name my``virtualenv`` after my project name for easy memory. It makes no sense to have a project called "cash_register" but the``virtualenv`` be called "fez". This is how I change to the right project after activating my ``virtualenv``. This goes in ``$WORKON_HOME/postactivate``

.. code-block:: bash

   #!/bin/bash
   # This hook is run after every virtualenv is activated.
   # if its a work or a personal project (Example)
   proj_name=$(echo $VIRTUAL_ENV|awk -F'/' '{print $NF}')
   if [[ -e "/Users/tsouza/PersonalProjects/$proj_name" ]]
   then
       cd ~/PersonalProjects/$proj_name
   else
       cd ~/WorkProjects/$proj_name
   fi

This about wraps up part one of this two part blog series. Next time I will discuss how to use Git and how to configure SublimeText2 and Aptana Studio for use with Python. Stay tuned!

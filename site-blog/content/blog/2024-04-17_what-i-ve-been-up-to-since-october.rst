What I've been up to since October
##################################
:author: tyrel
:category: Personal
:tags: personal
:status: published

Since my last post, about Djangocon I've been pretty MIA. I think I mentioned I got a job, at REDLattice. I've been doing some Django, and other misc programming, and it's been pretty neat. It's a security company, so I haven't really been able to talk about what I've been doing.

It's nice to be back where I'm comfortable, in my tech stack.

I also have been picking up some side projects again, taking some classes, running, and more.

I ran a 5k last month that was on a runway (KGSO) - that was fun. I got sub 30 minutes, but my heart rate spiked REALLY high because it was cold and rainy, not a pleasant race. I really need to work on my running, my average is usually 185, which for a 36yo is awful. I was running slowly for a bit and got it down to a 165 average, but took a week off from runnning because of hurting my foot with a cut.

Gustavo Pezzi released another course on his Pikuma platform - this time it's Playstation 1 programming in C and MIPS. I don't really have the best foundation in C despite doing C++ a lot in college and before - so I've picked up his Graphics Programming from Scratch course. I really enjoy the way he teaches, he never rushes ahead thinking you know something.

One thing I realized is in college I never really did much advanced algebra, so I've decided to also take a Linear Alebra course from MIT's Open Courseware MIT 18.06. Reading math text books is definitely different than I remember, but it might just be this professor. I wish I had some friends doing this course with me, I need to be held accountable.

For other projects I've gotten some new wood tools lately - I traded a watch for them - so I have some ideas in mind now that I have a planer.

Other than that, my daughter turned ONE last month! The day before I turned 36. We had a big party for me, her, and our niece. The party was fun, but I def am not the best party host.

We're taking our first flight as a family next week, hopefully Astrid's not too loud. I was this age when I flew down to Florida and we found out I had hearing issues, so I'm worried for that.

Not much else is new, but felt like it's been time for a blog post recently, even if it's just a personal post.

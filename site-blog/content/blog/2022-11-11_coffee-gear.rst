Coffee Gear
###########
:author: tyrel
:category: Coffee
:tags: coffee gear, coffee beans, coffee
:status: published


I put this up on my wiki a bit ago when a friend asked for coffee recommendations. Hopefully you can enjoy it and learn about some coffee machines you don't know yet.


************
What I drink
************

Woke Living Coffee 
==================

My favorite coffee is from Pamela and Marcus at https://wokelivingcoffee.com/. They are a couple of local to me roasters in Wake Forest, NC who have connections to a farm in La Dalia, Nicaragua. Not only do they sell great coffee, they are extremely nice and we visit them any chance we get at our local `Black Farmers Market <https://blackfarmersmkt.com/>`_.

****
Gear 
****

Grinders 
========

I prefer burr grinders, there's documented evidence that they are better, I won't get into that here. 

Baratza 
=======

I use a `Baratza Virtuoso <https://baratza.com/grinder/virtuoso/>`_ that I picked up refurbished. It works great! For drip coffee I will grind at step 28, for aeropress I will grind at 20, and for french press I will set to 30.

Hario 
=====

For travel I will bring my `Hario Skerton <https://smile.amazon.com/gp/product/B001802PIQ/>`_, works great, super easy to clean. I usually don't change the grind setting while traveling so I don't complain about the annoying screw post to set it.


****************
Brewing Machines 
****************

Technivorm 
==========

For my daily coffee, I have a `Moccamaster Technivorm <https://us.moccamaster.com/collections/glass-carafe-brewers/products/kbgv-select>`_. My friend Andrey recommended it. It works extremely well and very consistent pours.

Chemex
======

When I'm feeling fancy - or I'm trying a new coffee - I will break out my `Chemex <https://www.chemexcoffeemaker.com/eight-cup-classic-series-coffeemaker.html>`_. I do a 1:16 ratio of beans to water. I use the `Brown Paper Chemex Filters <https://www.chemexcoffeemaker.com/chemex-reg-bonded-filters-pre-folded-squares-natural.html>`_. I appreciate the bleached papers, but prefer unbleached.

Aeropress 
=========

For camping, I will bring my `Aeropress <https://aeropress.com/>`_. It's plastic, lightweight, and to my experience it is indestructible for travel.

French Press 
============

When I roast my own coffee, I like to experience it in multiple brewing methods. I have a `Bodum Bean French Press <https://luvtocook.com/products/ak11683-xyb-y16>`_ I got over a decade ago as a gift that has worked great. This one has an o-ring to seal the pouring spout, so the temperature chamber inside doesn't leak - a feature I like.

****************************
Roasting Machines & Software 
****************************

FreshRoast SR700 
================

I have a **glorified popcorn maker** `SR700 <https://www.roastmasters.com/sr700.html>`_ as a roaster. I'm not the biggest fan of it, the built in software is a mess, the manual buttons on it are a nightmare to use. It **works**, I can only get consistent coffee out of it if I use OpenRoast. It has a USB port so you can control it with software.

OpenRoast 
=========

The `OpenRoast <https://github.com/Roastero/Openroast>`_ software is okay, but I don't have a temperature probe on my SR700, so I can only see "what is set" for temperature, and not get an accurate reading if I were using something like Artisan. I could set up a PID server on an arduino and plug into the usb port, but I feel at that rate I'd rather just buy a new roaster that works with better software. I do like OpenRoast is in Python so I can read and write the code.


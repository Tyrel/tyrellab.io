Vertical Bars In Graphite
#########################
:date: 2012-02-08 15:10
:author: tyrel
:category: Tech
:tags: graphite, statsd
:slug: vertical-bars-in-graphite
:status: published

I am working with txStatsD and Graphite. I was having the hardest problem looking through the txStatsD code today finding how to graph something as an event, not a data point. I eventually went into every option on the graphite dashboard and found an option to make a bar.

.. figure:: {static}/images/2012/02/graphite-menu.png
   :alt: menu in graphite showing draw nonzero as infinite

This is the option that you must use when you want to mark events. For example we want to know "Server restarted", we would use this technique, as it doesn't make sense to aggregate "server restarted". Using nonzero as infinite is a good way to show an event took place.

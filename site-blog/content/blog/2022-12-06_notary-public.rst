Notary Public
#############
:author: tyrel
:category: Notary
:tags: notary
:status: published

Short update today.

I kept meaning to put together a way for people to utilize my notary services, so I finally made https://tyrel.bike/notary to redirect here to my Notary Public page.

I am a North Carolina Notary Public, and love helping my neighbors use my services.

If you're near me and need help - checkout `My Notary Page </pages/notary-public.html>`_.

Back Above Keene
################
:author: tyrel
:category: flying
:tags: flying
:status: published

.. figure:: {static}/images/2021/10/17_me_mom_n43337.jpg
   :alt: Mom and I hugging in front of the right wing of N43337

   Mom and I hugging in front of the right wing of N43337

It's my brother's wedding weekend and he asked me to take him and his new father-in-law flying. On October 16th I get to the airport, very well knowing that it's still too foggy for my 8am-10am flight that I was about to cancel in person. I still went in, because I haven't seen Beth in almost a year and wanted to say hi. We caught up, talked about how I've been going to Monadnock Aviation since I was in college - after my first flight on March 17, 2010! I called Levi and told him that we weren't flying, so he should just go finish getting ready for his wedding at 11:30.

The next day I had booked for my mother to go up the first time with me. We get to the airport at 9:55 and the FBO is locked up, Uhhhh. I email/text some people and no one knows how to get a hold of the desk attendant. We had apparently JUST missed David, one of the CFIs, who was getting into N44836 with a student I presume. So we waited a bit and it seems he was called away to do some fueling with the fuel truck. No fault of his at all, Sundays can get a little rough with only one person being at the FBOs desk.

We finally grab the book and keys and I go out and preflight. I'm in N43337 today, my favorite plane. I'm even wearing my "WARRIOR 337" shirt I got for soloing, for good luck! Preflight was easy as usual, once you've done it a hundred times in the same plane, you know what you're looking for from the checklists.

I waved to mom and Lauren to come over and we got in, I showed mom and Lauren how to set up the headphones and get into the back seat. Following a safety briefing we were off!

We taxied over from the Northwest ramp (The FBO area) to the East ramp and I did my run up there, the winds were 340° at 7kts gusting to 17, so the other planes were using runway 32. Waited around a bit for a radio check, the PTT button was sticky and no one was replying to my ask for radio checks, even though there were a few planes around, oh well.

We took off on runway 32 after waiting in line (it was busy today, wow!) and I did a lap around the pattern. It was not clean, I haven't flown in just over two months, and we had the added weight of a back passenger, so I was a little nervous the first pattern loop. But I will always do one pattern loop with a new passenger - just to give them a chance to bail out!

I'm not fond of runway 32, it has a high hill over Marcy Hill in Swanzey, NH and it always throws me off (probably because I have like 300 landings on runway 02 and maybe 15 on runway 32). The landing was fine, Lauren was taking pictures from the back and my mom said it was "wicked smooth, and you barely even felt the tires hit, I thought it was great, especially with the wind and everything" which as a first time passenger in a "oh my god I didn't know it was quite this small" airplane, I feel good about!

We then taxied back to 32, and took off again, this time it was a west departure out to Spofford Lake and over my mom's house in Chesterfield. I probably should have headed to Brattleboro first after Spofford. Had I done this, we would have been in position to fly Brattleboro, VT north to Putney, VT west of the Connecticut River and my mom would have gotten a MUCH nicer view of her house. Instead we flew over the lake and through a little valley over Westmoreland and went south along the river.

At one point I saw a dark cloud above and said "Okay we're about to go under a dark cloud, I'll try to avoid it but it might get turbulent", so mom and Lauren would know to hold on and probably 10 seconds later the plane went "woomp" down a bit because of turbulence - so they were prepared. After that small cloud we found a tiny patch of clean air and I turned to the right a little bit above Exit 3 and headed north again towards moms house, staying a little bit west of the river. I paralleled I91 again for a minute or two and then we got in line that I could fly close enough to mom's house that she could see it.

After that fun bit we headed back, directly over Spofford Lake for some more sights, and onward to KEEN.

We flew over Yale Forest, and I saw a cool cliff face I had never seen before - as we were entering RWY32 on a 45° entry. Entered the pattern and found myself VERY high (900msl at at 488msl airport) on final, on a 4000' runway with a displaced threshold so I executed a go around there. I probably could have made it, but with the wind and avoiding Marcy Hill, I figure it's always safe to Go Around.

The next loop I had my sight pictures again at runway 32 and we landed, rolled out to Taxiway Sierra and parked the plane!

Mom said it was fun!


.. figure:: {static}/images/2021/10/17_back-of-my-head.jpg
   :alt: Back of my head while turning a bit

   Back of my head while turning a bit

.. figure:: {static}/images/2021/10/17_cloudy-sun-view.jpg
   :alt: Cloudy and Sunny View off the right wing. Monadnock in the distance.

   Cloudy and Sunny View off the right wing. Monadnock in the distance.

.. figure:: {static}/images/2021/10/17_hannaford-kmart.jpg
   :alt: Turning over route 9 near Home Depot and Hannaford

   Turning over route 9 near Home Depot and Hannaford

.. figure:: {static}/images/2021/10/17_landing-32-14-on-final.jpg
   :alt: Cockpit view, a mile out from runway 32/14 with the runway in sight.

   Cockpit view, a mile out from runway 32/14 with the runway in sight.

.. figure:: {static}/images/2021/10/17_landing-32-14-short-final.jpg
   :alt: Cockpit view, a 1/4 mile from runway 32/14 with the runway in sight.

   Cockpit view, a 1/4 mile from runway 32/14 with the runway in sight.

.. figure:: {static}/images/2021/10/17_left-wing-looking-at-airport-and-monadnock.jpg
   :alt: Looking off the left wing at mount Monadnock.

   Looking off the left wing at mount Monadnock.

.. figure:: {static}/images/2021/10/17_sun-above-right-wing.jpg
   :alt: Just pretty sky views, out the right window above the right wing at the sun and clouds.

   Just pretty sky views, out the right window above the right wing at the sun and clouds.

.. figure:: {static}/images/2021/10/17_turning-from-backseat.jpg
   :alt: Three degree turn above moms house.

   Three degree turn above moms house.

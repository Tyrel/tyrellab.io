Brand New Server
################
:author: tyrel
:category: Blog
:tags: blog
:status: published

Per my last post, I did not succeed in cleaning off the malware.

That machine is dead and I am now running on a $4/mo Digital Ocean droplet - much less power than before, but I don't really need it anymore now that I have my own server at home.

I am sad I don't have a Pixelfed anymore, maybe I'll relaunch it some day.


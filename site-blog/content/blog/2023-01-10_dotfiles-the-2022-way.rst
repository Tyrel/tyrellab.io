Dotfiles - My 2022 Way
######################
:author: tyrel
:category: Tech
:tags: dotfiles, macos, linux, nix, ubuntu, 
:status: published

New Year's eve eve, my main portable computer crashed. Rebooting to Safe mode, I could mount this MacBook's hard drive long enough to SCP the files over the network to my server, but I had to start that over twice because it fell asleep. I don't have access to rsync in the "Network Recovery Mode" it seems - maybe I should look to see if next time I can install things, it's moot now.

I spent all January 1st evening working on learning how Nix works. Of course, I started with Nix on macOS (intel at least) so I had to also learn how nix-darwin works. I have my `dotfiles <https://gitea.tyrel.dev/tyrel/dotfiles>`_ set up to use Nix now, rather than an `INSTALL.sh` file that just sets a bunch of symlinks.

I played around for a litle bit with different structures, but what I ended up with by the end of the weekend was two bash scripts (still working on makefile, env vars are being funky) one for each operating system `rebuild-macos.sh` and `rebuild-ubuntu.sh`. For now I'm only Nixifying one macOS system and two Ubuntu boxes. Avoiding it on my work m1 Mac laptop, as I don't want to have to deal with managing `synthetic.conf` and mount points on a work managed computer. No idea how JAMF and Nix will fight.

My filetree currently looks like (trimmed out a host and a bunch of files in `home/`)

..  code-block:: none

    .
    ├── home
    │   ├── bin/
    │   ├── config/
    │   ├── gitconfig
    │   ├── gitignore
    │   ├── gpg/
    │   ├── hushlogin
    │   └── ssh/
    ├── hosts/
    │   ├── _common/
    │   │   ├── fonts.nix
    │   │   ├── home.nix
    │   │   ├── programs.nix
    │   │   └── xdg.nix
    │   ├── ts-tl-mbp/
    │   │   ├── brew.nix
    │   │   ├── default.nix
    │   │   ├── flake.lock
    │   │   ├── flake.nix
    │   │   ├── home-manager.nix
    │   │   └── home.nix
    │   └── x1carbon-ubuntu/
    │       ├── default.nix
    │       ├── flake.lock
    │       ├── flake.nix
    │       ├── home-manager.nix
    │       └── home.nix
    ├── rebuild-macos.sh
    └── rebuild-ubuntu.sh

Under `hosts/` as you can see, I have a `brew.nix <https://gitea.tyrel.dev/tyrel/dotfiles/src/branch/main/hosts/ts-tl-mbp/brew.nix>`_ file in my macbook pro's folder. This is how I install anything in homebrew. In my `flake.nix` for my macos folder I am using `home-manager`, `nix-darwin`, and `nixpkgs`. I provide this `brew.nix` to my `darwinConfigurations` and it will install anything I put in my `brew` nixfile.

I also have a `_common` directory in my `hosts`, this is things that are to be installed on EVERY machine. Things such as `bat`, `wget`, `fzf`, `fish`, etc. along with common symlinks and xdg-config links. My nvim and fish configs are installed and managed this way. Rather than need to maintain a neovim config for every different system, in the nix way, I can just manage it all in `_common/programs.nix`.

This is not "The Standard Way" to organize things, if you want more inspiration, I took a lot from my friend `Andrey's Nixfiles <https://github.com/shazow/nixfiles>`_. I was also chatting with him a bunch during this, so I was able to get three systems up and configured in a few days. After the first ubuntu box was configured, it was super easy to manage my others.

My `home/` directory is where I store my config files. My ssh public keys, my gpg public keys, my `~/.<dotfiles>` and my `~/.config/<files>`. This doesn't really need any explaination, but as an added benefit is I also decided to LUA-ify my nvim configs the same weekend. But that's a story for another time.

I am at this time choosing not to do NixOS - and relying on Ubuntu for managing my OS. I peeked into Andrey's files, and I really don't want to have to manage a full system configuration, drivers, etc. with Nix. Maybe for the future - when my Lenovo X1 Carbon dies and I need to reinstall that though.

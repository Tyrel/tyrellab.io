May 26, 2021 - First flight as a PPL
####################################
:author: tyrel
:category: flying
:tags: flying
:status: published

On April 26th, I took my first flight as a Private Pilot. I was flying with a CFI for a checkout to rent planes from the Wings of Carolina club, so the flight was nothing really to talk much about. It was really hot, and my first flight in almost 5 months, so I was definitely rusty.

Mostly using this flight as a stepping stone to try out the GPX viewer§. I flew a PA28-161 - Warrior II - N8080A for One hour. Flew from KTTA, up north west a bit, did a few steep turns, then back to KTTA.

§ - Not ported over, no JS on this blog.

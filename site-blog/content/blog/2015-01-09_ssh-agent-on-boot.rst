SSH Agent on "boot"
###################
:date: 2015-01-09 04:03
:author: tyrel
:category: Tech
:tags: linux, ssh
:slug: ssh-agent-on-boot
:status: published

I had a friend complain that he had to keep adding his ssh key to his ssh-agent every time he rebooted. I have a really easy bit of shell code you can put into your .bashrc or your .zshrc file:

.. code-block:: bash

  SSHKEYFILE=/path/to/your/ssh/key/file
  ssh-add -l | grep -q $SSHKEYFILE
  RC=$?
  if [ $RC -eq 1 ]
  then
      ssh-add -t 86400 $SSHKEYFILE
  echo "Added key internal to keychain."
  fi

This will check every time your bash or zsh rc file is sourced for your key in the currently added keys, and if it's not, it will add it.

This has the benefit of not requiring you to put in your password every time you connect to a remote machine if you password your ssh keys (which you should).

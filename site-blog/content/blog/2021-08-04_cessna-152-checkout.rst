Cessna 152 Checkout
###################
:author: tyrel
:category: flying
:tags: flying
:status: published

Today I got checked out in a Cessna 152. It was really my first time (besides spin training) flying high wing planes and I was a little nervous. We had a pretty standard pre-flight check, took a lot longer than just a "I'm out here to go on a flight", again because Luke was explaining things to me. There's a lot of differences like with the warrior, all the flaps are on hinges, but in the Cessna, there's one pulley hinge and some rollers you need to check, not just actuation hinges. Pointed out how the landing gear is different, there's plastic fairings that we need to repair quite frequently, and there are no dampeners.

After the pre-flight we took off, Vr is pretty low, at 50 so the plane just wanted to float almost immediately in my opinion. It was also a cooler day outside than my most recent few flights, so that helped too. We took off, headed north to the practice area. I get up to 3000' msl and we level off, trim for cruise flight and lean the engine. This plane also has a vernier mixture control, so it's nice to be able to dial in the mixture, even if there's no on screen display of the gallons per hour like the M20J. There were some clouds at 4000' I think, but they were tiny, and we figured we didn't need to go above them, so we stuck at the 3000' level.

This is where Luke says to just take the plane and do tiny things with it, like turn it uncoordinated, see how much rudder I'd need to do a turn. So I did that for a bit, did a standard rate turn to the left, and we switched to some 30° turns, leveled off then did two 45° turns, back to back. I hit my prop wash at the end of the second one, that always feels great. The first few turns were gross, this plane has old cables for the ailerons, so there's a lot of play when turning, so it took a few tries to be able to maintain altitude and find that comfortable dead spot with cable tension. The last plane (M20J) I flew, everything was much stiffer for turning.

After the few turns, he said "oh no, there goes your engine" and throttled to idle. I looked around for a place to land, and didn't see one immediately. One thing I could have done better is look out the back window, because the 152 actually has one! Well I found a spot to "crash land" so I brought the plane to Vglide (60) and slowly descended towards the place to land. I guess in my "everything is okay" did the A B part of the emergency checklist, but I didn't do ABCDE, so we did it again. This time I:

* A- pitched for best airspeed (60kts)
* B- found the best place to land
* C - Checklist, pretended to check fuel, master, key, primer, etc.
* D - pretended to switch to 121.5mHz and declare an emergency
* E- executed the landing, and prepared to exit (opened the door, turn off mixture, etc so the plane doesn't blow up and I can get out)

and found a cute little field to land in. That was successful so we headed back towards the airport.

At the airport everything was standard traffic pattern. There were three of us in the pattern, one tail dragger that was always just behind us, really friendly guy who liked chatting on frequency, and one low wing - maybe a warrior? - I can't remember. Well I did six landings. One normal landing, three short field (the last one I did great, landed on numbers and was done before A2 taxiway entrance) and then a few more normal.
Luke said that I was death gripping the yoke, so he did one lap around the airport - where he trimmed and only used his two fingers lightly to move the yoke. Watching him do that, I copied and did a lap around the pattern the same, much easier this time.

We then landed, taxied back and went to park. Parking is WILD, you sit on the tail and then back then walk backwards to get the plane in place. I guess when your plane's max ramp weight is 1675lbs, that's easy to do! Three times as much as my motorcycle… After we chatted, he said he feels safe with me flying it, and I agreed. "I feel safe, but not super comfortable, but that only comes with time so I feel safe to take it up and get more comfortable".

I then had to fill out the quiz, and scanned it, then emailed it to him.


.. figure:: {static}/images/2021/08/04_me-being-weird.jpg
   :alt: Picture of me in the cockpit being a weirdo with a big smirky mouth

   Picture of me in the cockpit being a weirdo with a big smirky mouth

.. figure:: {static}/images/2021/08/04_cessna-152-cockpit.jpg
   :alt: The cockpit of a Cessna 152, yokes, gauges, etc. Outside you can see other planes on the ramp.

   The cockpit of a Cessna 152, yokes, gauges, etc. Outside you can see other planes on the ramp.

.. figure:: {static}/images/2021/08/04_3d-track-1.jpg
   :alt:  Just a 3d diagram of the flight, not sure what I was focusing on here.

   Just a 3d diagram of the flight, not sure what I was focusing on here.

.. figure:: {static}/images/2021/08/04_3d-track-2.jpg
   :alt:  Just a 3d diagram of the flight, not sure what I was focusing on here.

   Just a 3d diagram of the flight, not sure what I was focusing on here.

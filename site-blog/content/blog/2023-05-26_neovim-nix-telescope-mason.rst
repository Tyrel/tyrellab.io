Neovim, Nix, Telescope, Tree-sitter, Mason
##########################################
:author: tyrel
:category: Tech
:tags: nix, nvim, rust
:status: published

I made a mistake with not reading CHANGELOGs for all my packages in Neovim this week. This sent me down a small rabbit hole trying to fix all the things.

What happened is I ran ``:PackerUpdate`` which, pulls the latest version of Packer packages, good, updates! But... Telescope has a new requirement on main branch that requires Neovim 0.9.0. The problem is that the latest NixPkgs for Neovim right now is 0.8.1. I ran to google, tried to set an overlay to use ``neovim-nightly``, but that didn't work. If you recall in `Dotfiles - My 2022 Way <https://tyrel.dev/blog/2023/01/dotfiles-my-2022-way.html>`_ I'm not actually using NixOS so (please correct me if I'm wrong) overlays don't work. I tried specifing a version in my ``programs.nix``, I tried a bunch of other things at 1AM that I don't remember anymore.

Almost ripped it all out just to use Nvim 0.9.0 on this machine until NixPkgs has updated the repo. I decided that was the wrong idea, and went to sleep.

Tonight, I was able to figure out that in Packer, you can pin a commit!

It's clear in the docs, but I was trying to fix it at the Nix level, so I didn't immediately think of this, even though at my last job, Tidelift, I was doing package pinning analysis! Derp.

So, I added ``commit="c1a2af0"``  to my ``use`` statment in ``plugins.lua`` and Telescope started working again without a warning, or issue. `Commit <https://gitea.tyrel.dev/tyrel/dotfiles/commit/eb32c2194aba355afec80e647bb4df31a3e40c73>`_.

That wasn't the only problem though. In my infinite wisdom, I followed some reddit posts that I won't link to, that suggested deleting ``~/.local/share/nvim`` and rerunning ``PackerInstall``, the problem there -- my tree-sitter configs are in my nix files.

This is an issue I need to look at later, but in my `programs.nix <https://gitea.tyrel.dev/tyrel/dotfiles/src/commit/eb32c2194aba355afec80e647bb4df31a3e40c73/hosts/_common/programs.nix#L26-L32>`_ file, I some reason have two entries of ``plugins =``. I had to uncomment the first one where I inject tree-sitter, and comment out the second setting. Then rebuild my nix flakes.

After that,I had to comment the first, uncomment the second, and rebuild with ``withAllGrammars`` config.

This worked, I had my rust tree-sitter configs working, but was missing ``rust-analyzer``.

That's in Mason! So I ran ``:Mason``, found ``rust-analyzer`` slapped that ``i`` button, and I finally had my system back after 2 days of issues.

This was mostly a blogpost so I can reference back to it in the future, but hopefully at least _someone_ learns to pin your dang nvim Packages!

Python Debugger
###############
:date: 2015-01-13 04:02
:author: tyrel
:category: Tech
:tags: python, pdb
:slug: python-debugger
:status: published

When I worked at Propel Marketing, my dev team used to have presentations on things they loved. I love the Python debugger. It's very useful and I believe a proper understanding of how to use a debugger, will make you a better programmer. Here is a presentation on the debugger I made for my team. https://prezi.com/cdc4uyn4ghih/python-debugger/

First Flight With My Dad
########################
:author: tyrel
:category: flying
:tags: flying
:status: published

I actually did a flight on July 7th, but It was only one lap around the pattern. It was 95°F here, and I knew I wouldn't be in the right mind to continue my flight as I got to the traffic pattern after take off, so I just called crosswind, and headed back to land, a whopping 0.3 hour flight time! So that's what just one melting traffic pattern loop looks like!

.. figure:: {static}/images/2021/07/10_loop.png
   :alt: GPS track around the TTA Airport, just one pattern loop

   GPS track around the TTA Airport, just one pattern loop

Anyway! My father was excited to fly with me, we took N2114F up. We intended to go from KTTA-SDZ-KRCZ, but it was still hot, so we decided to just do SDZ and back.

We take off, and do one lap around the pattern - I want this to be my standard when I bring new people up, it gives them a way out to say "GET ME OFF PLEASE". After that one pattern lap we headed towards SDZ. It was a really smooth flight, we flew at 4500 feet to the VOR, then tried to get to 7500 feet on the way back (We had time, I wanted to climb) but it was just SO HOT that we ended up just staying at 5500feet, the climb was taking forever!

When we were 10mi from the airport, it started getting super busy (BBQ day at the airport!), so we decided to waste some time in the practice area and I did a steep turn for him to waste more time. After that the traffic died down so we headed back to KTTA. I did a touch and go, a regular landing, and then being that there was no other traffic, practiced an emergency engine out procedure (what I failed on my checkride) and that went smoothly.

So nothing super special about this trip, besides it was my first time flying my father.


.. figure:: {static}/images/2021/07/10_tyrel-in-passenger-seat.jpg
   :alt: Me in the passenger seat climbing in and getting ready to go.

   Me in the passenger seat climbing in and getting ready to go.

.. figure:: {static}/images/2021/07/10_tyrel-and-tony.jpg
   :alt: Me and Dad sitting in airplane, somewhere above North Carolina

   Me and Dad sitting in airplane, somewhere above North Carolina

.. figure:: {static}/images/2021/07/10_tyrel-pointing-out-window.jpg
   :alt:  Me pointing out the tiny window on the airplane.

   Me pointing out the tiny window on the airplane.
   
.. figure:: {static}/images/2021/07/10_propeller-and-dashboard.jpg
   :alt: Part of the dashboard, and CO detector, plus a blurry propeller in the background

   Part of the dashboard, and CO detector, plus a blurry propeller in the background

.. figure:: {static}/images/2021/07/10_right-side-haze.jpg
   :alt: Looking out the right window, the sky is hazy, part of a wing can be seen

   Looking out the right window, the sky is hazy, part of a wing can be seen

.. figure:: {static}/images/2021/07/10_tony-and-tyrel.jpg
   :alt: Inside the cockpit, profile view of me, and part of my father's head.

   Inside the cockpit, profile view of me, and part of my father's head.

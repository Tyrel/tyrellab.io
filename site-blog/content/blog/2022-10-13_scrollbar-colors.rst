Scrollbar Colors
################
:date: 2022-10-13 12:07
:author: tyrel
:category: Website
:tags: css
:slug: scrollbar-colors
:status: published

Was talking to someone about CSS Nostalgia and "back in my day" when scrollbar colors came up.

.. code-block:: css

   /* For Chromium based browsers */
   ::-webkit-scrollbar {
     background: #2A365F;
   }
   ::-webkit-scrollbar-thumb {
     background: #514763;
   }

   /* For Firefox */
   html {
     scrollbar-color: #514763 #2A365F;
   }

Firefox and Chrome have different selectors, so in order to support the majority of browsers, you need both.

.. figure:: {static}/images/2022/10/scrollbar-chrome.png
   :alt: Chrome with a blue/purple scrollbar
   :width: 940px
   :height: 688px

   Chrome with a blue/purple scrollbar

.. figure:: {static}/images/2022/10/scrollbar-safari.png
   :alt: Safari with a blue/purple scrollbar

   Safari with a blue/purple scrollbar

.. figure:: {static}/images/2022/10/scrollbar-firefox.png
   :alt: Firefox with a blue/purple scrollbar

   Firefox with a blue/purple scrollbar

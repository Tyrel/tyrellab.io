Resume
######
:author: tyrel
:category: Resume
:slug: resume
:status: published



Senior Software Engineer with focus on Python, Django, Go. Technology enthusiast and IOT tinkerer. Private pilot. Amateur radio operator.

EXPERIENCE
----------

Mango Voice - Saint George, UT - Senior Software Engineer >>> November 2024 - Present (Remote)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

API and Scheduling Team

* Python, Django, Django Rest Framework - API development.
* just started, will update later!


REDLattice - Chantilly, VA — Senior Software Engineer >>> October 2023 -> November 2024 (Remote)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Products Team

* Developed a customer portal in Python/Django with multiple supporting microservices.
* Wrote Linux bash/python scripting to automate servers.
* Created and deployed microservices using docker containers.
* Added new GraphQL endpoints to microservices to allow interservice communication.
* Automated selenium workflows with GitLab CI to reduce manual testing time.
* Created an audit logging system to provide users with increased awareness of their systems.
* Created Debian packages for system services, reduces incorrect installation procedures for deployment.

EverQuote — Boston, MA — Senior Software Engineer >>> January 2022 - June 2023 (Remote)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Email Remarketing Team
  
  * Ported a Python2.7 monolith to Python3 microservices using FastAPI, and Kafka.
  * Automated a daily task for two analysts to be able to be run in only a few moments which saves 18 hours per week of analyst time.
  * Built a UI for manipulating database settings to aid in automating work for analysts.
  * Migrated a handful of repositories from CircleCI testing to GitHub Actions.
  * Ported multiple long running scripts and command line tools from Python 2.7 to Python 3.
  * Migrated alerts system platform from PagerDuty to Opsgenie

* Consumer Engagement Team

  * Launched a web service using Go and React (in TypeScript) to assist Sales Reps to provide alternative insurance company matches.
  * Transitioned multiple projects from Atlassian Bamboo to GitHub Actions.

Tidelift (acquired by Sonar 2024) — Boston, MA — Software Engineer III >>> April 2018 - December 2021
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* `Command Line Interface <https://tidelift.com/cli>`_

  * Developed binary CI/CD tool in Go to analyze software dependencies for security/licensing problems
  * Added ability to create users and manage repositories from the command line
  * Expanded personal project into company product

* Tidelift core redesign and pivot

  * Pivoted from solely a vulnerability scanner to supporting a catalog of open source dependencies
  * Part of small team developing Tidelift 2.0/3.0
  * Developed front end Vue, and back end Ruby on Rails
  * Worked on many Sinatra microservices.

* Expanded open source code under https://libraries.io to support more languages.

  * Added new languages manifests support
  * Programmed in multiple projects with code all available as open source on github
  * `Bibliothecary <https://github.com/librariesio/bibliothecary>`_: Added ability to detect dependencies in manifests for Poetry, pip-compile, pipfile,
  * `Conda Parser <https://github.com/librariesio/conda-parser>`_: Developed ingestor of Conda environment files, parsing environment.yml files
  * `Conda API <https://github.com/librariesio/conda-api>`_: Programmed web scraping of Anaconda to detect new packages, and REST API endpoints for packages


Addgene — Cambridge, MA — Software Engineer >>> March 2015 - March 2018
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*  Wrote code to support front end ecommerce site, and back end inventory management system using Django, Django Rest Framework, jQuery, Bootstrap
* Rewrote file storage backend to keep on Amazon S3 instead of local in-house file system

  * Released my own custom Django module as a result

* Trained non-developers in Python
* Helped start and expand a “non-developer developer” club
* Created mircoservices to support 3rd party API integration

  
Akamai — Cambridge, MA — Sr. Software Engineer >>> July 2014 - March 2015
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Wrote an API with Python/Flask that the front end could communicate with

  * Checked if email was a valid user in Salesforce, then sent the user an email from a template

* Rewrote the backend for an internal dashboard to load data in 0.1 second, down from 10 seconds

Propel Marketing (rebranded ThriveHive 2016) — Quincy, MA — Software Engineer >>> September 2012 - July 2014
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Built CMS to host and build hundreds of customer websites using Django, jQuery, Bootstrap
* Created custom widgets, custom styling, custom templates, and more


Appropriate Solutions Inc (Closed) — Peterborough, NH — Software Developer >>>  July 2010 - September 2012
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Built multiple client websites in Django, jQuery, Bootstrap.

SKILLS
------

* Python, Go, Django, Flask, Linux, Docker, Git, Mercurial, Postgres, MySQL, MariaDB, Amazon AWS (S3 mostly) Google Cloud Storage, REST APIs, Kafka, Redis, memcached, Sidekiq, RabbitMQ, Eclipse Mosquitto

EDUCATION
---------

* Keene State College - B.S. Applied Computer Science (Honors Society), Minor in Mathematics (Honors Society), Dean's List

PERSONAL PROJECTS
-----------------

* `Django DBFileStorage <https://gitlab.com/Tyrel/django-dbfilestorage>`_ Goal: to continue running CI tests on a remote storage when working on moving file storage to S3 without incurring additional AWS charges.


CONTRACTING WORK
----------------

Benchtop Devices
~~~~~~~~~~~~~~~~

* Multiple C# Desktop programs, interfacing via serial to get results about pressure tests, calculate decay rates, printing results to pdfs, label machines, and local Sqlite databases. Custom per each client.

  * Clients include: Waymo, FlexFlow, Hypertherm

* Python/Django and VueJS tool to convert pressure test results from the Cincinnati Test Blackbelt Machine, to PDFs.

VOLUNTEERING
------------

* Boston Athletic Association Amateur Radio Operator - 2019 Boston Marathon

Notary Public
=============
:date: 2022-12-06 00:00
:author: tyrel
:category: Notary
:slug: notary-public
:status: published


I am a North Carolina Notary Public, located in Durham, North Carolina.

I can perform notarial acts in all 100 counties.

If you want to use my services, I charge $5 per stamp (The maximum that NC allows you, and $0 for Absentee Ballots).


To prepare, first familiarize yourself with `How to get something notarized <https://www.nationalnotary.org/resources-for/public/how-to-prepare-for-notarization>`_.

Then, to request my services, `Email me <mailto:email@tyrel.dev>`_ or `Book Me <https://cal.com/tyrelsouza>`_


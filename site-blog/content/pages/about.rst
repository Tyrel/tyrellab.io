About
######
:date: 2022-05-01 00:00
:author: tyrel
:category: About
:slug: about
:status: published

About Tyrel
===========


Senior Software Engineer with a back end focus. Specializing in Python and Go.



Licenses
~~~~~~~~

* I have a Ham Radio License (Amateur Radio)
* I have a Restricted Radiotelephone Operator Permit (RR - for flying in certain countries)
* I have a General Mobile Radio Service (GMRS)
* I have a North Carolina Driver's License
* I have a North Carolina "Boater Education Card" (boating license for those of us born after 1988-01-31)
* I have a North Carolina Motorcycle Endorsement/License
* I have a Pilot's license
* I am an Ordained Minister at the Universal Life Church Monastery
* I am a North Carolina Notary Public - Commission expiring May 30, 2027


Site notes
~~~~~~~~~~

This blog is proudly powered by `Pelican <https://getpelican.com/>`_, which takes great advantage of Python.

The Theme is modified from `Blue Penguin Dark <https://github.com/tcarwash/blue-penguin-dark>`_ Theme

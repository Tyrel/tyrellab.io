Blogroll
########
:date: 2022-11-07 00:00
:author: tyrel
:category: Blogroll
:slug: blogroll
:status: published


* Blogs I Read

  * `Ned Batchelder's blog <https://nedbatchelder.com/blog>`_
  * `Andrey Petrov <https://shazow.net/>`_
  * `bitprophet.org on bitprophet.org <https://bitprophet.org/>`_
  * `bitquabit <https://www.bitquabit.com/>`_
  * `Julia Evans <http://jvns.ca>`_
  * `Nik Kantar <https://www.nkantar.com>`_
  * `Sam.Codes <https://blog.sam.codes/>`_
  * `Steven Leibrock <https://ste5e.site/>`_
  * `Stories by Jess Shapiro on Medium <https://medium.com/@transgingerjess?source=rss-44663d5275a4------2>`_
  * `The Industrious Rabbit - Blog <https://theindustriousrabbit.com>`_
  * `The Industrious Rabbit - Videos <https://www.youtube.com/channel/UCV37kgKv2uoFimfxaFKoXOA>`_
  * `__fredeb.dev <https://fredeb.dev/>`_


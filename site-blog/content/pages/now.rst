Now
###
:date: 2023-03-12 00:00
:author: tyrel
:category: Now
:slug: now
:status: published

`What is this? <https://nownownow.com/about>`_

March 2024
----------
* Astrid is now one, walking, and almost talking. Proud Dad.


October 2023
------------
* I started working at REDLattice, a Cybersecurity company.


June 2023
---------

* I was laid off from EverQuote in a round of layoffs.

April 2023
----------

* Set up FreeDOS on an old Dell Vostro 1720 - so I can now use things like TurboC, WordStar, and play old DOS Games.
* Set up Amiberry with AmigaOS Workbench3.1 on a flash drive, so I can tool around with Amiga finally.
* Ordered a Godot course on Zenva from HumbleBundle. I'm glad to finally peek at it at least, always interested me as a game engine.
* Astrid is two months old now, and not sleeping the best, but growing bigger.
* Making some new parent friends. Helped one new father move. Walked with a couple in our neighborhood about to have a baby and hopefully we stay connected.
* Grandmother is out of the hospital after having two heart stints put in, and giving us a scare with potential fatal surgery.

March 2023
~~~~~~~~~~

* Wife had our first child Astrid Mina, on March 2, 2023. On Parental leave until April 26.
* Still working at EverQuote, on a different team now that does Go, React and Ruby.
* Haven't flown in a while, taking some time off because of some `mental health reasons <https://k3tas.radio/airband/2022/10/24/david-dezendorf/>`_, and now with a kid, no time for a bit.


What I'm Listening To
---------------------

<a href="https://last.fm/user/tacidsky"><img src="https://lastfm-recently-played.vercel.app/api?user=tacidsky" alt="last.fm recent played"></a>

Active Projects
###############
:date: 2022-10-16 00:00
:author: tyrel
:category: Tech
:slug: active-projects
:status: draft

I'm always tooling around on a couple things in the background to keep myself fresh with some programming languiages.

* `itor <https://gitea.tyrel.dev/tyrel/itor>`_ - a flashcard tool, create and practice with flash cards. 100% meant as a BubbleTea learning tool for TUIs in Go.

* `Frank Tank <https://gitea.tyrel.dev/tyrel/frank_tank.git>`_ - Manipulating a water heater from a NodeMCU and Stepper motor.

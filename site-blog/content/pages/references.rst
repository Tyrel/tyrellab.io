References
##########
:date: 2022-11-04 00:00
:author: tyrel
:category: References
:slug: references
:status: draft

Blog Citations
~~~~~~~~~~~~~~

* 2023

  * Another citation from my friend Nik. `Self Hosting: Resolved? <https://nkantar.com/blog/2023/04/self-hosting-resolved/>`_

* 2020

  * My friend Nik has a blog, and I pointed out an alternative so he `Cited Me <https://www.nkantar.com/blog/dict-setdefault-rocks/>`_.

Work Blogs
~~~~~~~~~~

* 2021

  * I wrote a blog post about our `Command Line Interface <https://blog.tidelift.com/hey-software-engineers-the-tidelift-subscription-has-command-line-integration-now>`_ that we are building at Tidelift. (Note: I am no longer employed by Tidelift nor work on this project)


Interviews
~~~~~~~~~~

* 2021

  * `Daily Tar Heel - Covid19 Standby List Twitter Opinions <https://www.dailytarheel.com/article/2021/03/university-standby-covid-19-vaccine>`_ (Nothing special, just gave my opinion on UNC's Standby Covid Vaccinations nearby.)


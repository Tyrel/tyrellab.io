from pathlib import Path

pwd = Path.cwd()
AUTHOR = 'Tyrel Souza'
SITENAME = "Tyrel's Blog"
TWITTER_USERNAME = 'tyrelsouza'
SITEURL = ''
SITESUBTITLE = 'Code, Flying, Tech, Automation'

SITEBASE = "/blog"


MENUITEMS = [
    ('Home', f'{SITEBASE}/'),
    ('Tags', f'{SITEBASE}/tags.html'),
    ('Categories', f'{SITEBASE}/categories.html'),
]
LINKS = (
    ('All My Accounts', 'https://tyrel.dev/links'),
    ('Book Me', 'https://cal.com/tyrelsouza'),
)

PATH = 'content'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
# FEED_DOMAISITEURL
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

FEED_ATOM = 'tyrel-dev.atom.xml'
#FEED_RSS = 'tyrel-dev.rss'
FEED_ALL_ATOM = 'tyrel-dev.all.xml'
#FEED_ALL_RSS = 'tyrel-dev.all.rss'


PLUGINS = ["webassets"]

DEFAULT_PAGINATION = False
DISPLAY_CATEGORIES_ON_MENU = False
DELETE_OUTPUT_DIRECTORY = True

STATIC_PATHS = (
    'images',
    'extras',
    )

ARTICLE_PATHS = ['blog', ]
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_URL = 'posts/{date:%Y}/{date:%m}/index.html'


DEFAULT_METADATA = {
    'status': 'draft',
    'extras/favicon.ico': {'path': 'favicon.ico'},
}

THEME = "./themes/solarized"

DISPLAY_PAGES_ON_MENU=True

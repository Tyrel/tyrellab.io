from pathlib import Path

pwd = Path.cwd()
AUTHOR = 'Tyrel Souza'
SITENAME = "Tyrel's Blog"
TWITTER_USERNAME = 'tyrelsouza'
SITEURL = ''
SITESUBTITLE = 'Code, Flying, Tech, Automation'
FAVICON = '/img/icon192.png'

SITEBASE = "/blog"


MENU_INTERNAL_PAGES = [
    ('Archive', 'archives.html', f'archives.html'),
    #('Tags', 'tags.html', f'tags.html'),
    #('Categories', 'categories.html', f'categories.html'),
]

MENUITEMS = [
    ('My Code', 'https://gitlab.com/Tyrel'),
    ('Find Me', '/links'),
    ('Book Me', 'https://cal.com/tyrelsouza'),
    ('Tags', '/blog/tags.html'),
    ('Categories', '/blog/categories.html'),
]

PATH = 'content'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
# FEED_DOMAISITEURL
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

FEED_ATOM = 'tyrel-dev.atom.xml'
FEED_ALL_ATOM = 'tyrel-dev.all.xml'


PLUGINS = ["webassets"]

DEFAULT_PAGINATION = True
DISPLAY_CATEGORIES_ON_MENU = False
DELETE_OUTPUT_DIRECTORY = True

STATIC_PATHS = (
    'images',
    'extras',
    )

ARTICLE_PATHS = ['blog', ]
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_URL = 'posts/{date:%Y}/{date:%m}/index.html'


DEFAULT_METADATA = {
    'status': 'draft',
    'extras/favicon.ico': {'path': 'favicon.ico'},
}

SOCIAL = ()

# THEME = "./themes/pelican-themes/gum"
DISPLAY_PAGES_ON_MENU=True
LANDING_PAGE_ABOUT=True
THEME = pwd / "themes" / "blue-penguin-dark"
#THEME = pwd / "themes" / "solarized"

PAGINATED_TEMPLATES = {'index': 10, 'tag': 10, 'category': 10, 'author': None}
DISPLAY_HOME = False
SUMMARY_MAX_LENGTH = 400

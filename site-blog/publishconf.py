# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *
SITEBASE = "/blog"

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'https://tyrel.dev/blog'
RELATIVE_URLS = False

FEED_ATOM = 'tyrel-dev.atom.xml'
FEED_RSS = None
FEED_ALL_ATOM = 'tyrel-dev.all.xml'
FEED_ALL_RSS = None


DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""
